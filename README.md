# jlisher-package-template

This is a package template for future rpm packages

## Development

This project uses the [jlisher-package-template](https://gitlab.com/jlisher/jlisher-package-template) (more documentation is needed, will get to that). However, it provides a few simple options to adjust the basic packaging and publishing options, though
it may seem like double work, so probably will to change soon.

As there is no code to compile in this project, there is no build phase. We simply have to package the source files (structured for simpler development) in an installable layout, then publish our package.

### File Based Options

The following files provide a way to adjust our packaging and publishing variables. These files can be committed to a VCS, allowing an architecture or OS (which has special requirements) to maintain an independent branch in an obvious way. These values
should correspond with the values set in [jlisher-package-template.spec](./jlisher-package-template.spec). At the moment, you need to manually set the values.

- `.name`:
  - The name to use for the package
  - Usage:
    - Used by all scripts in [./scripts](./scripts)
    - To create and determine the file name and path of:
      - The Source tarball
      - The RPM `.spec` file
      - The RPM package file
      - The SRPM package file
      - The remote directory for the source and package publishing
- `.version`
  - The current version of the package
  - Usage:
    - Used by all scripts in [./scripts](./scripts)
    - To create and determine the file name and path of:
      - The Source tarball
      - The RPM package file
      - The SRPM package file
- `.arch`
  - The current architecture we are packaging for
  - Usage:
    - Used by [./scripts/publish-rpms.sh](./scripts/publish-rpms.sh)
    - To determine the file name and path of:
      - The RPM package file
      - The SRPM package file
- `.build`
  - The distribution release version
  - Usage:
    - Used by [./scripts/publish-rpms.sh](./scripts/publish-rpms.sh)
    - To determine the file name and path of:
      - The RPM package file

### Packaging

The following is some instructions on how you can go about preparing to package the source. You could always use the [srpm releases](https://rpms.jlisher.com/src/jlisher-package-template/)
or [source releases](https://sources.jlisher.com/jlisher-package-template/), if you don't want the extra publishing scripts (see [Publishing](#Publishing) below).

#### Packaging Dependencies:

- rpm-build
- tar
- gzip
- realpath

Please review [build-source-tar.sh](./scripts/build-source-tar.sh), [build-rpm.sh](./scripts/build-rpm.sh), and [jlisher-package-template.spec](./jlisher-package-template.spec) for information on building.

### Publishing

Most people probably won't care too much about the publishing scripts, provided in the [jlisher-package-template git repo](https://gitlab.com/jlisher/jlisher-package-template). They likely don't fit most use cases, but it is simple enough for my needs.

#### Publishing Dependencies:

- [sign-and-sum](https://gitlab.com/jlisher/sign-and-sum)
- openssh-clients

Please review [publish-source.sh](./scripts/publish-source.sh) and [publish-rpms.sh](./scripts/publish-rpms.sh) for more information.
